jQuery(document).ready(function($) {

	// preload effect
	$('body').addClass('loaded');

	// split image slider
	$('.split__slider--button').on('click', function(){
		if ( !$(this).hasClass('active') ) {
			var slideId = $(this).data('slide');
			$('.split__slider--button, .split__slide').removeClass('active');
			$(this).addClass('active');
			$('.split__slide[data-slide='+slideId+']').addClass('active');
		}
	});

	$('.split__slide').on('click', function(){
		var slideId = $(this).data('slide');
		if ( slideId == 3 ) {
			$(this).removeClass('active')
			$('.split__slider--button').removeClass('active');
			$('.split__slide[data-slide=1], .split__slider--button[data-slide=1]').addClass('active');
		}
		else {
			$(this).removeClass('active');
			$('.split__slider--button[data-slide='+slideId+']').removeClass('active');
			slideId = slideId + 1;
			$('.split__slide[data-slide='+slideId+'], .split__slider--button[data-slide='+slideId+']').addClass('active');
		}
	});

	// faq accordion
	$('.single-faq__title').on('click', function(){
		$(this).toggleClass('active');
		$(this).next('.single-faq__text').slideToggle(300);
	});

	$('.mobile-burger').on('click', function(){
		$('.mobile-burger, .mobile-menu').toggleClass('active');
	});

	$( '.mobile-menu' ).on( 'mousewheel DOMMouseScroll', function ( e ) {
	    var e0 = e.originalEvent,
	        delta = e0.wheelDelta || -e0.detail;
	    e.preventDefault();
	});

	$('.login-slide').first().addClass('active');

	setInterval(function(){
		if ( !$('.login-slide.active').next('.login-slide').length ) {
			$('.login-slide.active').removeClass('active');
			$('.login-slide').first().addClass('active');
		}
		else {
			$('.login-slide.active').removeClass('active').next('.login-slide').addClass('active');
		}
		var bgColor = $('.login-slide.active').data('bgcolor');
		$('.login-slider').css('background',bgColor);
	}, 8000);
	
});

// header background animation
$(window).on('scroll', function(){
	if ( $(window).scrollTop() >= 40 ) {
		$('.header__top').addClass('scrolled');
	}
	else {
		$('.header__top').removeClass('scrolled');	
	}
});

// smooth scrolling to anchor
$(function() {
  $('a[href*="#"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 1000);
        return false;
      }
    }
  });
});